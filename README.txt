CKEditor Leaflet
================

Description
===========
Accordions are a popular way to structure content. In the past nested divs or
definition lists were augmented by JavaScript to provide this functionality,
but the HTML5 details element provides this functionality natively. This module
adds a button so editors can easily use this element in their content.

Usage
=====
Go to the Text formats and editors settings (/admin/config/content/formats) and
add the Details button to any CKEditor-enabled text format you want.

Installation
============
1. Download the plugin. The official version is at
   http://ckeditor.com/addon/leaflet but I have forked a newer version at
   https://github.com/mandclu/ckeditor-leaflet to ensure compatibility with
   Google Maps API requirements, and to provide additional configurability of
   defaults through the Drupal admin UI.
2. Place the plugin in the root libraries folder (/libraries) and ensure that
   the plugin folder is named leaflet.
3. Enable CKEditor Leaflet Plugin module in the Drupal admin.
4. In one or more text formats, drag the Leaflet button (looks like a map pin)
   into the toolbar. You will now see a new vertical tab in the "CKeditor
   plugin settings" section below. You can use this to set any preferred map
   defaults, but the most important field is for the Google API Key. The
   original plugin provided a default key, but should configure your own here.
   Note that the key must have the appropriate Google Maps APIs enabled.

Each filter format will now have a config tab for this plugin.

Dependencies
============
This module requires the core CKEditor module, as well as ckeditor_lineutils
and ckeditor_widget.

Uninstallation
==============
1. Uninstall the module from 'Administer >> Modules'.

MAINTAINERS
===========
Martin Anderson-Clutz - https://www.drupal.org/u/mandclu

Credits
=======

Initial development and maintenance by Digital Echdina.
