<?php

namespace Drupal\ckeditor_leaflet\Plugin\CKEditorPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ckeditor\CKEditorPluginConfigurableInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "leaflet" plugin.
 *
 * @CKEditorPlugin(
 *   id = "leaflet",
 *   label = @Translation("Leaflet Map"),
 *   module = "ckeditor_leaflet"
 * )
 */
class LeafletPlugin extends PluginBase implements CKEditorPluginConfigurableInterface, CKEditorPluginButtonsInterface {

  use StringTranslationTrait;

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getDependencies().
   */
  public function getDependencies(Editor $editor) {
    return ['lineutils', 'widget'];
  }

  /**
   * Get path to library folder.
   */
  public function getLibraryPath() {
    $path = 'libraries/leaflet';
    if (\Drupal::moduleHandler()->moduleExists('libraries')) {
      $path = libraries_get_path('leaflet');
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->getLibraryPath() . '/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return ['core/drupal.jquery'];
  }

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::isInternal().
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    $config = [];
    $settings = $editor
      ->getSettings();
    if (!isset($settings['plugins']['leaflet']['api_key'])) {
      return $config;
    }
    return [
      'leaflet_maps_google_api_key' => $settings['plugins']['leaflet']['api_key'],
      'map_zoom' => $settings['plugins']['leaflet']['map_zoom'],
      'map_alignment' => $settings['plugins']['leaflet']['map_alignment'],
      'map_responsive' => $settings['plugins']['leaflet']['map_responsive'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state, Editor $editor) {

    // Defaults.
    $config = [
      'api_key' => '',
      'map_zoom' => 10,
      'map_alignment' => 'center',
      'map_responsive' => 'on',
    ];
    $settings = $editor
      ->getSettings();
    if (isset($settings['plugins']['leaflet'])) {
      $config = $settings['plugins']['leaflet'];
    }
    $form['api_key'] = [
      '#title' => t('Google API Key'),
      '#type' => 'textfield',
      '#default_value' => $config['api_key'],
      '#description' => t('A valid Google Maps API key that will be used to perform the geolocation lookups.'),
    ];
    // TODO: Provide configuration options for default zoom, size, etc.

    // Prevent unprivileged users from changing the base path or standard.
    if (!\Drupal::currentUser()->hasPermission('administer site configuration')) {
      $form['api_key']['#disabled'] = TRUE;
      $form['api_key']['#description'] .= ' ' . $this->t('Only editable by Administrators!');
    }

    $options = [];
    for ($i = 1; $i <= 20; $i++) {
      $options[$i] = $i;
    }

    $form['map_zoom'] = [
      '#title' => t('Default Map Zoom'),
      '#type' => 'select',
      '#default_value' => $config['map_zoom'],
      '#options' => $options,
    ];

    $form['map_alignment'] = [
      '#title' => t('Default Map Alignment'),
      '#type' => 'select',
      '#default_value' => $config['map_alignment'],
      '#options' => [
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
      ],
    ];

    $form['map_responsive'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Responsive Map'),
      '#default_value' => $config['map_responsive'],
      '#description' => $this->t('Make the map full width, for better display on mobile devices.'),
      '#return_value' => 'on',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'leaflet' => [
        'label' => t('Add map'),
        'image' => $this->getLibraryPath() . '/icons/leaflet.png',
      ],
    ];
  }

}
